﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Sallah.Core.Domain.Entities;
using Sallah.Core.Entities;
using Sallah.Infrastructure.Data.Entity.Persistence;
using Sallah.Infrastructure.Data.Identity;

namespace Sallah.Infrastructure.Data.Persistence
{
    public static class ApplicationDbContextSeed
    {
        public static async Task SeedDefaultUserAsync(UserManager<AppUser> userManager)
        {
            var defaultUser = new AppUser { UserName = "administrator@localhost", Email = "administrator@localhost" };

            if (userManager.Users.All(u => u.UserName != defaultUser.UserName))
            {
                await userManager.CreateAsync(defaultUser, "Administrator1!");
            }
        }

        public static async Task SeedProductDataAsync(ApplicationDbContext context)
        {
            // Seed, if necessary
            if (!context.Products.Any())
            {
                context.Products.AddRange(new List<Product>
                {
                   new Product
                   {
                       CategoryId = 1,
                       Description = "white ram",
                       FrontView = "https://www.google.com/search?q=sallah+ram+pictures&sxsrf=ALeKk02BniDm3Ci6jOkPuwf8K5RJe6Nd8w:1595696285935&tbm=isch&source=iu&ictx=1&fir=UlZxLIXJt0YQ9M%252COvrkv-LZonif5M%252C_&vet=1&usg=AI4_-kQj0s0KAPs4d7oh4gw79caKekhcdA&sa=X&ved=2ahUKEwiVlKvy7-jqAhUFilwKHRfDBM0Q9QEwAHoECAoQGg&biw=1139&bih=720#imgrc=ZCiYaqw1FkNHNM",
                       BackSideView = "https://www.google.com/search?q=sallah+ram+pictures&sxsrf=ALeKk02BniDm3Ci6jOkPuwf8K5RJe6Nd8w:1595696285935&tbm=isch&source=iu&ictx=1&fir=UlZxLIXJt0YQ9M%252COvrkv-LZonif5M%252C_&vet=1&usg=AI4_-kQj0s0KAPs4d7oh4gw79caKekhcdA&sa=X&ved=2ahUKEwiVlKvy7-jqAhUFilwKHRfDBM0Q9QEwAHoECAoQGg&biw=1139&bih=720#imgrc=ZCiYaqw1FkNHNM",
                       LeftSideView = "https://www.google.com/search?q=sallah+ram+pictures&sxsrf=ALeKk02BniDm3Ci6jOkPuwf8K5RJe6Nd8w:1595696285935&tbm=isch&source=iu&ictx=1&fir=UlZxLIXJt0YQ9M%252COvrkv-LZonif5M%252C_&vet=1&usg=AI4_-kQj0s0KAPs4d7oh4gw79caKekhcdA&sa=X&ved=2ahUKEwiVlKvy7-jqAhUFilwKHRfDBM0Q9QEwAHoECAoQGg&biw=1139&bih=720#imgrc=ZCiYaqw1FkNHNM",
                       RightSideView = "https://www.google.com/search?q=sallah+ram+pictures&sxsrf=ALeKk02BniDm3Ci6jOkPuwf8K5RJe6Nd8w:1595696285935&tbm=isch&source=iu&ictx=1&fir=UlZxLIXJt0YQ9M%252COvrkv-LZonif5M%252C_&vet=1&usg=AI4_-kQj0s0KAPs4d7oh4gw79caKekhcdA&sa=X&ved=2ahUKEwiVlKvy7-jqAhUFilwKHRfDBM0Q9QEwAHoECAoQGg&biw=1139&bih=720#imgrc=ZCiYaqw1FkNHNM",
                       Name = "ram",
                       OwnerId  = 1,
                       ProductId =  Guid.NewGuid(),
                       VideoLink = "",
                   },
                   new Product
                   {
                       CategoryId = 1,
                       Description = "Black ram",
                        FrontView = "https://www.google.com/search?q=sallah+ram+pictures&sxsrf=ALeKk02BniDm3Ci6jOkPuwf8K5RJe6Nd8w:1595696285935&tbm=isch&source=iu&ictx=1&fir=UlZxLIXJt0YQ9M%252COvrkv-LZonif5M%252C_&vet=1&usg=AI4_-kQj0s0KAPs4d7oh4gw79caKekhcdA&sa=X&ved=2ahUKEwiVlKvy7-jqAhUFilwKHRfDBM0Q9QEwAHoECAoQGg&biw=1139&bih=720#imgrc=ZCiYaqw1FkNHNM",
                       BackSideView = "https://www.google.com/search?q=sallah+ram+pictures&sxsrf=ALeKk02BniDm3Ci6jOkPuwf8K5RJe6Nd8w:1595696285935&tbm=isch&source=iu&ictx=1&fir=UlZxLIXJt0YQ9M%252COvrkv-LZonif5M%252C_&vet=1&usg=AI4_-kQj0s0KAPs4d7oh4gw79caKekhcdA&sa=X&ved=2ahUKEwiVlKvy7-jqAhUFilwKHRfDBM0Q9QEwAHoECAoQGg&biw=1139&bih=720#imgrc=ZCiYaqw1FkNHNM",
                       LeftSideView = "https://www.google.com/search?q=sallah+ram+pictures&sxsrf=ALeKk02BniDm3Ci6jOkPuwf8K5RJe6Nd8w:1595696285935&tbm=isch&source=iu&ictx=1&fir=UlZxLIXJt0YQ9M%252COvrkv-LZonif5M%252C_&vet=1&usg=AI4_-kQj0s0KAPs4d7oh4gw79caKekhcdA&sa=X&ved=2ahUKEwiVlKvy7-jqAhUFilwKHRfDBM0Q9QEwAHoECAoQGg&biw=1139&bih=720#imgrc=ZCiYaqw1FkNHNM",
                       RightSideView = "https://www.google.com/search?q=sallah+ram+pictures&sxsrf=ALeKk02BniDm3Ci6jOkPuwf8K5RJe6Nd8w:1595696285935&tbm=isch&source=iu&ictx=1&fir=UlZxLIXJt0YQ9M%252COvrkv-LZonif5M%252C_&vet=1&usg=AI4_-kQj0s0KAPs4d7oh4gw79caKekhcdA&sa=X&ved=2ahUKEwiVlKvy7-jqAhUFilwKHRfDBM0Q9QEwAHoECAoQGg&biw=1139&bih=720#imgrc=ZCiYaqw1FkNHNM",
                       Name = "ram",
                       OwnerId  = 1,
                       ProductId =  Guid.NewGuid(),
                       VideoLink = "",
                   }

                }); ;

                await context.SaveChangesAsync();
            }
        }


        public static async Task SeedCategoryDataAsync(ApplicationDbContext context)
        {
            // Seed, if necessary
            if (!context.Categories.Any())
            {
                context.Categories.AddRange(new List<Category>
                {
                   new Category
                   {
                       CategoryName = "Ram",
                   },
                    new Category
                   {
                       CategoryName = "Cow"
                   }

                }); ;

                await context.SaveChangesAsync();
            }
        }

        public static async Task SeedCountryDataAsync(ApplicationDbContext context)
        {
            // Seed, if necessary
            if (!context.Countries.Any())
            {
                context.Countries.AddRange(new List<Country>
                {
                   new Country
                   {
                       CallCode = "+234",
                       CountryName = "Nigeria"
                   }


                }); ;

                await context.SaveChangesAsync();
            }
        }

        public static async Task SeedStateDataAsync(ApplicationDbContext context)
        {
            // Seed, if necessary
            if (!context.States.Any())
            {
                context.States.AddRange(new List<State>
                {
                   new State
                   {
                       CountryId = 1,
                       StateName = "Lagos",
                   }


                }); ;

                await context.SaveChangesAsync();
            }
        }

        public static async Task SeedOwnerDataAsync(ApplicationDbContext context)
        {
            // Seed, if necessary
            if (!context.Owners.Any())
            {
                context.Owners.AddRange(new List<Owner>
                {
                   new Owner
                   {
                       Address = "lagos, yaba",
                       Email = "theowner@gmail.com",
                       LastName = "Lawal",
                       UserName = "abdul",
                       StateId = 1,
                       FirstName = "Abdulateef",
                        PhoneNumber = "08182367005",
                        isComfirmed = true

                   },
                   new Owner
                   {
                       Address = "lagos, yaba",
                       Email = "owner@gmail.com",
                       LastName = "Hassan",
                       UserName = "abdul",
                       StateId = 1,
                       FirstName = "Tobi",
                        PhoneNumber = "07182367005",
                        isComfirmed = false

                   }



                }); ;

                await context.SaveChangesAsync();
            }
        }
    }
}
