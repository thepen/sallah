﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sallah.Infrastructure.Data.Persistence;

namespace Sallah.Infrastructure.Data.Entity.Persistence.Repository
{
    public abstract class Repository<TEntity> where TEntity : class
    {
        protected ApplicationDbContext Context;
        private readonly DbSet<TEntity> _entitySet;

        protected Repository(ApplicationDbContext context)
        {
            Context = context;
            _entitySet = context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            _entitySet.Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            _entitySet.AddRange(entities);
        }

        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _entitySet.Where(predicate).AsNoTracking();
        }

        public TEntity GetSingle(Expression<Func<TEntity, bool>> predicate)
        {
            return _entitySet.SingleOrDefault(predicate);
        }


        public Task<List<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _entitySet.Where(predicate).ToListAsync();
        }
        public Task<IQueryable<TEntity>> QueryAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return Task.Run(() => _entitySet.Where(predicate).AsQueryable());
        }
        public Task<int> GetDataCountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _entitySet.CountAsync(predicate);
        }

        public Task<bool> ExistAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _entitySet.AnyAsync(predicate);
        }

        public TEntity Get(int entityId)
        {
            return _entitySet.Find(entityId);
        }

        public TEntity Get(Guid entityGuid)
        {
            return _entitySet.Find(entityGuid);
        }

        public async Task<TEntity> GetAsync(int entityId)
        {
            return await _entitySet.FindAsync(entityId);
        }

        public async Task<TEntity> GetAsync(Guid entityGuid)
        {
            return await _entitySet.FindAsync(entityGuid);
        }

        public Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _entitySet.SingleOrDefaultAsync(predicate);
        }

        public void Remove(TEntity entity)
        {
            _entitySet.Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            _entitySet.RemoveRange(entities);
        }
    }

}
