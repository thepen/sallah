﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Sallah.Core.Common.Exceptions;
using Sallah.Core.Common.Interface;
using Sallah.Core.Common.QueryCommands.Products.Queries.Dto;
using Sallah.Core.Entities;
using Sallah.Core.Interfaces.Gateways.Repository;
using Sallah.Infrastructure.Data.Persistence;

namespace Sallah.Infrastructure.Data.Entity.Persistence.Repository
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;
        private readonly ICategoryRepository _categoryRepository;

        public ProductRepository(ApplicationDbContext context, IMapper mapper, ICurrentUserService currentUserService, ICategoryRepository categoryRepository) : base(context)
        {
            _mapper = mapper;
            _currentUserService = currentUserService;
            _categoryRepository = categoryRepository;

        }
        public Task<ProductDto> Create(ProductDto productDto)
        {
            
            var category = Context.Find<Category>(productDto.CategoryId);
            if (category == null )
            {
                throw new GenericException("category id is required");
            }
            var product = _mapper.Map<Product>(productDto);
            product.ProductId = Guid.NewGuid();
            product.LastModifiedBy = _currentUserService.UserId;
            product.CreatedBy = _currentUserService.UserId;
            category.Products.Add(product);
             Context.SaveChanges();
            var result = _mapper.Map<ProductDto>(product);
            return Task.FromResult(result);
        }

        public async Task<bool> DeleteProduct(int productId)
        {
            var product = await Context.Products.FindAsync(productId);
            if (product != null)
            {
                Context.Products.Remove(product);
                Context.SaveChanges();
                return true;
            }
            return false;
        }

        public async Task<IEnumerable<ProductDto>> FindByOwnerId(int ownerId)
        {
            var products = await Context.Products.Where(x => x.OwnerId == ownerId)
                                 .ProjectTo<ProductDto>(_mapper.ConfigurationProvider)
                                 .OrderBy(t => t.Id)
                                 .ToListAsync();
            return products;
        }

        public async Task<ProductDto> FindByProductId(Guid productId)
        {
            var product = await Context.Products.SingleOrDefaultAsync(x => x.ProductId == productId);
            if (product != null)
            {
                var productDto = _mapper.Map<ProductDto>(product);
                return productDto;
            }
            return new ProductDto();
        }
        public async Task<IEnumerable<ProductDto>> FindByCategoryId(int categoryId)
        {
            var products = await Context.Products.Where(x => x.CategoryId == categoryId)
                                           .ProjectTo<ProductDto>(_mapper.ConfigurationProvider)
                                           .OrderBy(t => t.Id)
                                           .ToListAsync();
            return products;
        }
        public async Task<IEnumerable<ProductDto>> GetAllProducts()
        {
            var products = await Context.Products
                                            .ProjectTo<ProductDto>(_mapper.ConfigurationProvider)
                                            .OrderBy(t => t.Id)
                                            .ToListAsync();
            return products;
        }

        public async Task<bool> Update(int productId, ProductDto product)
        {
            var result = await Context.Products.SingleOrDefaultAsync(x => x.Id == productId);
            if (result != null)
            {
                var mappedProduct = _mapper.Map<Product>(product);
                Context.Update(mappedProduct);
                await Context.SaveChangesAsync();
                return true;
            }
            return false;
        }

       
    }
}
