﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Sallah.Core.DTOS.Category;
using Sallah.Core.Entities;
using Sallah.Core.Interfaces.Gateways.Repository;
using Sallah.Infrastructure.Data.Entity.Persistence.Repository;
using System.Linq;

namespace Sallah.Infrastructure.Data.Persistence.Repository
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        private readonly IMapper _mapper;

        public CategoryRepository(ApplicationDbContext context, IMapper mapper) : base(context)
        {
            _mapper = mapper;
        }

        public async Task<string> Create(string name)
        {
            var existingcategory = FindByName(name);
            if (existingcategory != null)
            {
                return "";
            }
            Category category = new Category
            {
                CategoryName = name,
            };
            Context.Add(category);
            await Context.SaveChangesAsync();
            return name;
        }

        public  Task<Category> FindById(int Id)
        {
            var existingcategory = Context.Categories.SingleOrDefault(x => x.Id == Id);
            if (existingcategory != null)
            {
                return Task.FromResult(existingcategory);

            }

            return Task.FromResult(new Category()); 

        }

        public Task<Category> FindByName(string name)
        {
            var result = Context.Categories.Where(x => x.CategoryName == name);
            return Task.FromResult(result.FirstOrDefault());
        }
    }
}