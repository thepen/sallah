﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Sallah.Core.Common.Models;

namespace Sallah.Infrastructure.Data.Identity
{
    public static class IdentityResultExtensions
    {
        public static Result ToApplicationResult(this IdentityResult result)
        {
            return result.Succeeded
                ? Result.Success()
                : Result.Failure(result.Errors.Select(e => e.Description));
        }
    }
}
