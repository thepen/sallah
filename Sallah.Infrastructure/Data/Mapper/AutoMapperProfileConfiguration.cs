﻿using System;
using AutoMapper;
using Sallah.Core.Common.QueryCommands.Products.Queries.Dto;
using Sallah.Core.Entities;

namespace Sallah.Infrastructure.Data.Mapper
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration()
        : this("Profile")
        {

        }

        public AutoMapperProfileConfiguration(string profileName) : base(profileName)
        {
            CreateMap<ProductDto, Product>();
            CreateMap<Product, ProductDto>();
        }
    }
}
