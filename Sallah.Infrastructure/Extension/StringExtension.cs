﻿using System;
using System.ComponentModel.DataAnnotations;
using Sallah.Infrastructure.Helpers;

namespace Sallah.Infrastructure.Extension
{
    public static class StringExtensions
    {
        public static string Encrypt(this string textToEncrypt)
        {
            return CryptManager.Encrypt(textToEncrypt);
        }

        public static string Decrypt(this string textToDecrypt)
        {
            return CryptManager.Decrypt(textToDecrypt);
        }

        public static string EncryptSHA512(this string textToEncrypt)
        {
            return CryptManager.EncrpyptSHA512String(textToEncrypt);
        }

        public static bool IsValidEmail(this string email)
        {
            var e = new EmailAddressAttribute();
            return (!string.IsNullOrEmpty(email) && e.IsValid(email));
        }
    }
}
