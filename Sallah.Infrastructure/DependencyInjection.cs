﻿using System;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sallah.Core.Common.Interface;
using Sallah.Core.Interfaces.Gateways.Repository;
using Sallah.Infrastructure.Auth;
using Sallah.Infrastructure.Data;
using Sallah.Infrastructure.Data.Entity.Persistence.Repository;
using Sallah.Infrastructure.Data.Identity;
using Sallah.Infrastructure.Data.Mapper;
using Sallah.Infrastructure.Data.Persistence;
using Sallah.Infrastructure.Data.Persistence.Repository;

namespace Sallah.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            if (configuration.GetValue<bool>("UseInMemoryDatabase"))
            {
                services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseInMemoryDatabase("Sallah"));
            }
            else
            {
                services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(
                        configuration.GetConnectionString("DefaultConnection"),
                        b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));
            }

            services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());

            services.AddDefaultIdentity<AppUser>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

          

            services.AddScoped<IIdentityService, IdentityService>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IJwtFactory, JwtFactory>();


            services.AddAuthentication()
                .AddIdentityServerJwt();
            //automapper 
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfileConfiguration());
            });

            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);
            return services;
        }
    }
}
