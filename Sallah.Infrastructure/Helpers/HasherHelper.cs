﻿using System;
using Sallah.Infrastructure.Extension;

namespace Sallah.Infrastructure.Helpers
{
    public static class HasherHelper
    {
        public static string Hash(string text)
        {
            return text.Encrypt();

        }

        public static string Unhash(string textToUnhash)
        {
            return textToUnhash.Decrypt();
        }


        public static bool VerifyHashedPassword(string hashedText, string providedText)
        {
            bool isValidPassword = false;
            if (hashedText != Hash(providedText))
                return isValidPassword;

            return true;
        }
    }
}
