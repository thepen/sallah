﻿using System;
namespace Sallah.Core.DTOS.Category
{
    public class CategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
