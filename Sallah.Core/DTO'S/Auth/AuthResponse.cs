﻿using System;
namespace Sallah.Core.DTOS.Auth
{
    public class AuthResponse
    {
        public Token Token { get; set; }
    }
}
