﻿using System;
namespace Sallah.Core.DTOS.Products
{
    public class ProductDto
    {
        public int Id { get; set; }
        public Guid ProductId { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int OwnerId { get; set; }
        public string Description { get; set; }
        public string ImageLink1 { get; set; }
        public string ImageLink2 { get; set; }
        public string ImageLink3 { get; set; }
        public string ImageLink4 { get; set; }
        public string VideoLink { get; set; }
    }
}
