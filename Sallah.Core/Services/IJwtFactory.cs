﻿using System;
using System.Threading.Tasks;
using Sallah.Core.DTOS;

namespace Sallah.Core.Services
{
    public interface IJwtFactory
    {
        Task<Token> GenerateEncodedToken(string id, string userName);
    }
}
