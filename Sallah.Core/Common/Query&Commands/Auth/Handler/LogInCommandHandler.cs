﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sallah.Core.Common.Interface;
using Sallah.Core.Common.QueryCommands.Auth.Commands;
using Sallah.Core.DTOS.Auth;

namespace Sallah.Core.Common.QueryCommands.Auth.Handler
{
    public class LogInCommandHandler : IRequestHandler<LogInCommand, AuthResponse>
    {

        private readonly IIdentityService _identityService;
        public LogInCommandHandler(IIdentityService identityService)
        {
            _identityService = identityService;
                 
        }

        public async Task<AuthResponse> Handle(LogInCommand request, CancellationToken cancellationToken)
        {
            var token = await _identityService.Authenticate(request.logIn.UserName, request.logIn.Password);
            AuthResponse authResponse = new AuthResponse
            {
                Token = token
            };
            return authResponse;
        }
    }
}
