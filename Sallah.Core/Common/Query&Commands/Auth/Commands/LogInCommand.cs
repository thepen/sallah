﻿using System;
using MediatR;
using Sallah.Core.DTOS.Auth;

namespace Sallah.Core.Common.QueryCommands.Auth.Commands
{
    public class LogInCommand : IRequest<AuthResponse>
    {
        public LogIn logIn { get; set; }
    }
}
