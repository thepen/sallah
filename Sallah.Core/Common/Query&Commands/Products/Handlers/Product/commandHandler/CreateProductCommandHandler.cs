﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sallah.Core.Common.QueryCommands.Products.Commands.CreateProduct;
using Sallah.Core.Common.QueryCommands.Products.Queries.Dto;
using Sallah.Core.Interfaces.Gateways.Repository;

namespace Sallah.Core.Common.QueryCommands.Products.Handlers.Product.commandHandlers
{
    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, ProductDto>
    {
        private readonly IProductRepository _productRepository;
        public CreateProductCommandHandler(IProductRepository productRepository  )
        {
            _productRepository = productRepository;
        }
        public async Task<ProductDto> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            return await _productRepository.Create(request.ProductDto);
        }
    }
}
