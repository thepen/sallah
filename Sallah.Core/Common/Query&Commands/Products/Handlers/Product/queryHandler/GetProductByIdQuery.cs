﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sallah.Core.Common.QueryCommands.Products.Queries.Dto;
using Sallah.Core.Common.QueryCommands.Products.Queries.GetProductByIdQuery;
using Sallah.Core.Interfaces.Gateways.Repository;

namespace Sallah.Core.Common.QueryCommands.Products.Handlers.Product.queryHandler
{
    public class GetProductByIdQueryHandler : IRequestHandler<GetProductByIdQuery, ProductDto>
    {
        private readonly IProductRepository _productRepository;

        public GetProductByIdQueryHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;

        }

        public async Task<ProductDto> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            var products = await _productRepository.FindByProductId(request.Id);
            return products;
        }
    }

}
