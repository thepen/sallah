﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sallah.Core.Common.QueryCommands.Products.Queries.Dto;
using Sallah.Core.Common.QueryCommands.Products.Queries.GetAllProducts;
using Sallah.Core.Interfaces.Gateways.Repository;

namespace Sallah.Core.Common.QueryCommands.Products.Handlers.Product.queryHandler
{
    public class GetAllProductsQueryHandler : IRequestHandler<GetAllProductsQuery, IEnumerable<ProductDto>>
    {
        private readonly IProductRepository _productRepository;

        public GetAllProductsQueryHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public async Task<IEnumerable<ProductDto>> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
        {
            var products = await _productRepository.GetAllProducts();
            return products;
        }
    }
}
