﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sallah.Core.Common.QueryCommands.Products.Queries.Dto;
using Sallah.Core.Common.QueryCommands.Products.Queries.get;
using Sallah.Core.Interfaces.Gateways.Repository;

namespace Sallah.Core.Common.QueryCommands.Products.Handlers.Product.queryHandler
{
    public class GetProductsByOwnerQueryHandler : IRequestHandler<GetProductsByOwnerQuery, IEnumerable<ProductDto>>
    {
        private readonly IProductRepository _productRepository;

        public GetProductsByOwnerQueryHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;

        }

        public async Task<IEnumerable<ProductDto>> Handle(GetProductsByOwnerQuery request, CancellationToken cancellationToken)
        {
            var products = await _productRepository.FindByCategoryId(request.CategoryId);
            return products;
        }
    }

}
