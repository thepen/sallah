﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sallah.Core.Common.QueryCommands.Products.Queries.Dto;
using Sallah.Core.Interfaces.Gateways.Repository;

namespace Sallah.Core.Common.QueryCommands.Products.Commands.CreateProduct
{
    public  class CreateProductCommand : IRequest<ProductDto>
    {
        public CreateProductCommand(ProductDto productDto)
        {
            ProductDto = productDto;
        }
       public ProductDto ProductDto { get;  }
        
    }

}
