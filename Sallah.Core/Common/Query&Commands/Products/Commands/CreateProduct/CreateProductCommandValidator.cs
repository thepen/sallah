﻿using System;
using FluentValidation;
using Sallah.Core.Common.Interface;

namespace Sallah.Core.Common.QueryCommands.Products.Commands.CreateProduct
{
    public class CreateProductCommandValidator : AbstractValidator<CreateProductCommand>
    {
        private readonly IApplicationDbContext _context;
        public CreateProductCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.ProductDto.Description)
                .NotEmpty().WithMessage("a brief description is required.")
                .MaximumLength(300).WithMessage("Title must not exceed 200 characters.");
            RuleFor(v => v.ProductDto.OwnerId)
                .NotEmpty().WithMessage("owner Id is required.");
            RuleFor(v => v.ProductDto.CategoryId)
                .NotEmpty().WithMessage("Category Id is required.");
            RuleFor(v => v.ProductDto.RightSideView)
                .NotEmpty().WithMessage("Right side view image  is required.");
            RuleFor(v => v.ProductDto.LeftSideView)
              .NotEmpty().WithMessage("Left side view image  is required.");
            RuleFor(v => v.ProductDto.BackSideView)
              .NotEmpty().WithMessage("Back side view image  is required.");
        }
    }
}
