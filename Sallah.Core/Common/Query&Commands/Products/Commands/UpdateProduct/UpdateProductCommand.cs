﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sallah.Core.Common.QueryCommands.Products.Queries.Dto;
using Sallah.Core.Interfaces.Gateways.Repository;

namespace Sallah.Core.Common.QueryCommands.Products.Commands.UpdateProduct
{
   
    public partial class UpdateProductCommand : IRequest<bool>
    {
        public UpdateProductCommand( )
        {
            ProductDto = new  ProductDto();
        }
        public int Id { get;  }
        public ProductDto ProductDto { get; }
    }

    public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, bool>
    {
        private readonly IProductRepository _productRepository;
        public UpdateProductCommandHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public async Task<bool> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            var result = await _productRepository.Update(request.Id, request.ProductDto);
            return result;
        }
    }
}
