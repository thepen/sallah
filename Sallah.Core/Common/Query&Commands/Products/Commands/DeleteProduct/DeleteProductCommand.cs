﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sallah.Core.Interfaces.Gateways.Repository;

namespace Sallah.Core.Common.QueryCommands.Products.Commands.DeleteProduct
{
   
    public partial class DeleteProductCommand : IRequest<bool>
    {
        
        public int Id { get; }
    }

    public class UpdateProductCommandHandler : IRequestHandler<DeleteProductCommand, bool>
    {
        private readonly IProductRepository _productRepository;
        public UpdateProductCommandHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
       
        public async Task<bool> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            var result = await _productRepository.DeleteProduct(request.Id);
            return result;
        }
    }
}
