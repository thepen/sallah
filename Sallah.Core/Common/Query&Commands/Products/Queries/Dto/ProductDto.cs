﻿using System;
namespace Sallah.Core.Common.QueryCommands.Products.Queries.Dto
{
    public class ProductDto 
    {
        public int Id { get; set; }
        public Guid ProductId { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int OwnerId { get; set; }
        public string Description { get; set; }
        public string FrontView { get; set; }
        public string LeftSideView { get; set; }
        public string RightSideView { get; set; }
        public string BackSideView { get; set; }
        public string VideoLink { get; set; }
    }
}
