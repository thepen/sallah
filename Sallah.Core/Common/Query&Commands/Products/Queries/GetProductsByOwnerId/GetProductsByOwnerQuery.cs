﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Sallah.Core.Common.QueryCommands.Products.Queries.Dto;
using Sallah.Core.Interfaces.Gateways.Repository;

namespace Sallah.Core.Common.QueryCommands.Products.Queries.get
{

    public class GetProductsByOwnerQuery : IRequest<IEnumerable<ProductDto>>
    {
        public int CategoryId { get; set; }
    }
}
