﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Sallah.Core.Common.Interface;
using Sallah.Core.Common.QueryCommands.Products.Queries.Dto;
using Sallah.Core.Entities;
using Sallah.Core.Interfaces.Gateways.Repository;

namespace Sallah.Core.Common.QueryCommands.Products.Queries.GetAllProducts
{
    public class GetAllProductsQuery : IRequest<IEnumerable<ProductDto>>
    {
        
    }

}
