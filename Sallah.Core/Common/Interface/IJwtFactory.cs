﻿using System;
using System.Threading.Tasks;
using Sallah.Core.DTOS;

namespace Sallah.Core.Common.Interface
{
    public interface IJwtFactory
    {
        Task<Token> GenerateEncodedToken(string id, string userName);
    }
}
