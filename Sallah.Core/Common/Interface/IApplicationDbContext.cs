﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sallah.Core.Domain.Entities;
using Sallah.Core.Entities;

namespace Sallah.Core.Common.Interface
{
    public interface IApplicationDbContext
    {
        DbSet<Product> Products { get; set; }
        DbSet<State> States { get; set; }
        DbSet<Category> Categories { get; set; }
        DbSet<Country> Countries { get; set; }
        DbSet<Owner> Owners { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken));

    }
}
