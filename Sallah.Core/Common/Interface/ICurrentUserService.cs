﻿using System;
namespace Sallah.Core.Common.Interface
{
    public interface ICurrentUserService
    {
        string UserId { get; }
    }
}
