﻿using System;
using System.Threading.Tasks;
using Sallah.Core.Common.Models;
using Sallah.Core.DTOS;

namespace Sallah.Core.Common.Interface
{
    public interface IIdentityService
    {
        Task<string> GetUserNameAsync(string userId);
        Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password);
        Task<Result> DeleteUserAsync(string userId);
        Task<Token> Authenticate(string userName, string  password);

    }
}
