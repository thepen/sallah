﻿using System;
using System.Threading.Tasks;
using Sallah.Core.DTOS.Category;
using Sallah.Core.Entities;

namespace Sallah.Core.Interfaces.Gateways.Repository
{
    public interface ICategoryRepository
    {
        Task<string> Create(string name );
        Task<Category> FindByName(string name);
        Task<Category> FindById(int Id );


    }
}
