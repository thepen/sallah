﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sallah.Core.Common.QueryCommands.Products.Queries.Dto;
using Sallah.Core.Entities;

namespace Sallah.Core.Interfaces.Gateways.Repository
{
    public interface IProductRepository
    {
        Task<ProductDto> Create(ProductDto product);
        Task<bool> Update(int Id,ProductDto product);
        Task<IEnumerable<ProductDto>> FindByOwnerId(int ownerId);
        Task<IEnumerable<ProductDto>> FindByCategoryId(int categoryId);
        Task<IEnumerable<ProductDto>> GetAllProducts();
        Task<ProductDto> FindByProductId(Guid productId);
        Task<bool> DeleteProduct(int productId);
    }
}
