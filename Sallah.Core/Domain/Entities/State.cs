﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Sallah.Core.Domain.Common;
using Sallah.Core.Entities;

namespace Sallah.Core.Domain.Entities
{
    public class State : AuditableEntity
    {
        
        public int Id { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(100)]
        public string StateName { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public ICollection<Owner> Owners { get; set; }
    }
}
