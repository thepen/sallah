﻿using System;
using System.ComponentModel.DataAnnotations;
using Sallah.Core.Domain.Common;

namespace Sallah.Core.Entities
{
    public class Product  : AuditableEntity
    {
        public int Id { get; set; }
        public Guid ProductId { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(100)]
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public int OwnerId { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(500)]
        public string Description { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string FrontView { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string LeftSideView { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string RightSideView { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string BackSideView { get; set; }
        public string VideoLink { get; set; }

    }
}
