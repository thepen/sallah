﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sallah.Core.Domain.Common;

namespace Sallah.Core.Entities
{
    public class Category : AuditableEntity
    {
        public Category()
        {
            Products = new HashSet<Product>();
        }
        public int Id { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(100)]
        public string  CategoryName { get; set; }
        public ICollection<Product> Products { get; set; } 
    }
}

