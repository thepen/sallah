﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Sallah.Core.Domain.Common;

namespace Sallah.Core.Domain.Entities
{
    public class Country : AuditableEntity
    {
        public Country()
        {
            States = new HashSet<State>();
        }
        public int Id { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(100)]
        public string CountryName { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(10)]
        public string CallCode { get; set; }
        public ICollection<State> States { get; set; }
    }
}
