﻿using System;
using System.ComponentModel.DataAnnotations;
using Sallah.Core.Domain.Common;
using Sallah.Core.Domain.Entities;

namespace Sallah.Core.Entities
{
    public class User : AuditableEntity
    {
        public string Id { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(100)]
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(500)]
        public string Address { get; set; }
        public int StateId { get; set; }
        public State State { get; set; }
    }
}
