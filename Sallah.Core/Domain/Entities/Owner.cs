﻿using System;
using System.ComponentModel.DataAnnotations;
using Sallah.Core.Domain.Common;

namespace Sallah.Core.Domain.Entities
{
    public class Owner : AuditableEntity
    {
        public int Id { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(500)]
        public string Address { get; set; }
        public int StateId { get; set; }
        public State State { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        public string PhoneNumber { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        public string FirstName { get; set; }
        public bool isComfirmed { get; set; }
    }
}
