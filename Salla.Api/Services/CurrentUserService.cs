﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Sallah.Core.Common.Interface;

namespace Salla.Api.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            UserId = httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier);
        }

        public string UserId { get; }
    }
}
