﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sallah.Core.Common.QueryCommands.Products.Commands.CreateProduct;
using Sallah.Core.Common.QueryCommands.Products.Commands.UpdateProduct;
using Sallah.Core.Common.QueryCommands.Products.Queries.Dto;

namespace Salla.Api.Controllers
{
    // [Authorize]
    [Route("api/[controller]")]
    public class ProductController  : ApiController
    {     
        [HttpPost("Create")]
        public async Task<ActionResult<ProductDto>> Create([FromBody]ProductDto productDto )
        {
            var result = await  Mediator.Send(new CreateProductCommand(productDto));
            return  Ok(result);
        }
        
        [HttpPut("Update")]
        public async Task<ActionResult<bool>> Update(int id, UpdateProductCommand command)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

           return await Mediator.Send(command);

        }

    }
}
